use std::fs::{read, write};
use std::path::PathBuf;

fn main() {
    let py = read("cloud-init.py").unwrap();
    let py = String::from_utf8_lossy(&py).to_string();

    let mut path: PathBuf = std::env::var("OUT_DIR").unwrap().into();
    path.push("cloud-init.rs");
    write(&path, format!("r#\"{}\"#\n", py)).unwrap();
}
