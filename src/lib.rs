//! Virtual machine administration under [libvirt][].
//!
//! vmadm is a library and a tool to create and destroy virtual
//! machines running under a local libvirt. Virtual machines are
//! described in specification  files, using YAML:
//!
//!
//! ~~~yaml
//! foo:
//!   cpus: 4
//!   memory_mib: 4096
//!   image_size_gib: 100
//!
//! bar:
//!   cpus: 1
//!   memory_mib: 512
//!   image_size_gib: 1
//! ~~~
//!
//! All the machines in a specification file are created or destroyed at
//! once.
//!
//! [libvirt]: https://libvirt.org/

pub mod cloudinit;
pub mod cmd;
pub mod config;
pub mod image;
pub mod install;
pub mod libvirt;
pub mod progress;
pub mod spec;
pub mod sshkeys;
pub mod util;
