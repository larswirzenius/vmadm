//! The `delete` sub-command.

use crate::libvirt::{Libvirt, VirtError};
use crate::progress::Progress;
use crate::spec::Specification;

/// Delete VMs corresponding to specifications.
///
/// Delete the VM corresponding to each specification provided by the caller.
pub fn delete(specs: &[Specification], progress: &Progress) -> Result<(), VirtError> {
    progress.chatty("deleting virtual machines");

    let libvirt = Libvirt::connect("qemu:///system")?;
    for spec in specs {
        progress.chatty(&format!("asking virtual machine {} to shutdown", spec.name));
        libvirt.trigger_shutdown(&spec.name)?;
    }
    for spec in specs {
        progress.step(&format!("deleting virtual machine {}", spec.name));
        libvirt.shutdown(&spec.name)?;
        libvirt.delete(&spec.name, &spec.image)?;
    }

    progress.chatty("deletion successful");
    Ok(())
}
