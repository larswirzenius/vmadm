//! The `list` sub-command.

use crate::config::Configuration;
use crate::libvirt::{Libvirt, VirtError};
use crate::progress::Progress;

/// The `list` sub-command.
///
/// Return the names of all the virtual machines existing on the
/// libvirt instance.
pub fn list(_config: &Configuration, progress: &Progress) -> Result<(), VirtError> {
    progress.chatty("listing virtual machines");
    let libvirt = Libvirt::connect("qemu:///system")?;
    let mut names = libvirt.names()?;
    names.sort();
    for name in names {
        println!("{}", name);
    }
    Ok(())
}
