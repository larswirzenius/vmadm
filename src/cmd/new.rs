//! The `new` sub-command.

use crate::cloudinit::{CloudInitConfig, CloudInitError};
use crate::image::{ImageError, VirtualMachineImage};
use crate::install::{virt_install, VirtInstallArgs, VirtInstallError};
use crate::libvirt::{Libvirt, VirtError};
use crate::progress::Progress;
use crate::spec::Specification;
use crate::util::wait_for_ssh;

use bytesize::GIB;
use log::debug;
use tempfile::tempdir;

/// Errors returned by this module.
#[derive(Debug, thiserror::Error)]
pub enum NewError {
    /// Problem with cloud-init configuration.
    #[error(transparent)]
    CloudInitError(#[from] CloudInitError),

    /// Problem creating VM image.
    #[error(transparent)]
    ImageError(#[from] ImageError),

    /// Problem with libvirt.
    #[error(transparent)]
    VirtInstallError(#[from] VirtInstallError),

    /// Problem with virsh.
    #[error(transparent)]
    VirshError(#[from] std::io::Error),

    /// Problem from libvirt server.
    #[error(transparent)]
    VirtError(#[from] VirtError),
}

/// The `new` sub-command.
///
/// Create all the new virtual machines specified by the caller. Wait
/// until each VM's SSH port listens for connections.
pub fn new(specs: &[Specification], progress: &Progress) -> Result<(), NewError> {
    progress.chatty("Creating new virtual machines");

    let libvirt = Libvirt::connect("qemu:///system")?;
    for spec in specs {
        progress.step(&format!("creating new VM {}", spec.name));

        progress.chatty("creating cloud-init config");
        let init = CloudInitConfig::from(spec)?;
        debug!("finished creating cloud-init config");

        progress.chatty(&format!(
            "creating VM image {} from {}",
            spec.image.display(),
            spec.base.display()
        ));
        debug!("creating VM image");
        let image = VirtualMachineImage::new_from_base(&spec.base, &spec.image)?;
        debug!("finished creating VM image");

        progress.chatty(&format!("resizing image to {} GiB", spec.image_size_gib));
        image.resize(spec.image_size_gib * GIB)?;

        progress.chatty("creating VM");
        let dir = tempdir()?;
        let iso = dir.path().join("cloudinit.iso");
        let mut args = VirtInstallArgs::new(&spec.name, &image, &init);
        args.set_memory(spec.memory_mib);
        args.set_vcpus(spec.cpus);
        for network in spec.networks.iter() {
            args.add_network(network);
        }
        virt_install(&args, &iso)?;
    }

    for spec in specs {
        progress.chatty(&format!("waiting for {} to open its SSH port", spec.name));
        wait_for_ssh(&spec.name);

        libvirt.detach_cloud_init_iso(&spec.name)?;
        libvirt.set_autostart(&spec.name, spec.autostart)?;
    }

    progress.chatty("creation successful");
    Ok(())
}
