//! The `recreate` sub-command.

use crate::cmd::delete::delete;
use crate::cmd::new::{new, NewError};
use crate::libvirt::VirtError;
use crate::progress::Progress;
use crate::spec::Specification;

/// Errors returned by this module.
#[derive(Debug, thiserror::Error)]
pub enum RecreateError {
    /// Problem with new.
    #[error(transparent)]
    New(#[from] NewError),

    /// Problem from libvirt server.
    #[error(transparent)]
    VirtError(#[from] VirtError),
}

/// The `recreate` sub-command.
///
/// This deletes, then news virtual machines.
pub fn recreate(specs: &[Specification], progress: &Progress) -> Result<(), RecreateError> {
    progress.chatty("Re-creating virtual machines");

    delete(specs, progress)?;
    new(specs, progress)?;

    progress.chatty("re-creation successful");
    Ok(())
}
