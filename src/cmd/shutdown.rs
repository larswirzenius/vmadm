//! The `shutdown` sub-command.

use crate::libvirt::{Libvirt, VirtError};
use crate::progress::Progress;
use crate::spec::Specification;

/// Shut down VMs corresponding to specifications.
pub fn shutdown(specs: &[Specification], progress: &Progress) -> Result<(), VirtError> {
    progress.chatty("shutting down virtual machines");

    let libvirt = Libvirt::connect("qemu:///system")?;
    for spec in specs {
        progress.step(&format!("shutting down virtual machine {}", spec.name));
        libvirt.trigger_shutdown(&spec.name)?;
    }
    for spec in specs {
        progress.chatty(&format!("waiting for {} to become inactive", spec.name));
        libvirt.wait_for_inactive(&spec.name)?;
    }

    progress.chatty("shut down OK");
    Ok(())
}
