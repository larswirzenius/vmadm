//! The `reboot` sub-command.

use crate::cmd::shutdown::shutdown;
use crate::cmd::start::{start, StartError};
use crate::libvirt::VirtError;
use crate::progress::Progress;
use crate::spec::Specification;

/// Errors returned by this module.
#[derive(Debug, thiserror::Error)]
pub enum RebootError {
    /// Problem with new.
    #[error(transparent)]
    New(#[from] StartError),

    /// Problem from libvirt server.
    #[error(transparent)]
    VirtError(#[from] VirtError),
}

/// The `recreate` sub-command.
///
/// This shuts down, then starts virtual machines.
pub fn reboot(specs: &[Specification], progress: &Progress) -> Result<(), RebootError> {
    progress.chatty("Restarting virtual machines");

    shutdown(specs, progress)?;
    start(specs, progress)?;

    progress.chatty("Restart successful");
    Ok(())
}
