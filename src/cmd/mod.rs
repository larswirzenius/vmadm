//! Implementations of command line sub-commands.
//!
//! This module has sub-modules with functions for all the
//! sub-commands of the command line tool part of the crate.

pub mod new;
pub use new::new;

pub mod recreate;
pub use recreate::recreate;

pub mod list;
pub use list::list;

pub mod delete;
pub use delete::delete;

pub mod start;
pub use start::start;

pub mod shutdown;
pub use shutdown::shutdown;

pub mod cloud_init;
pub use cloud_init::cloud_init;

pub mod config;
pub use config::config;

pub mod spec;
pub use spec::spec;

pub mod reboot;
pub use reboot::reboot;
