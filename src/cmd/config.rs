//! The `config` sub-command.

use std::io::Write;

use crate::config::Configuration;
use crate::progress::Progress;

/// The `config` sub-command.
///
/// Write the actual run-time configuration to stdout as JSON. We
/// convert the config to JSON to make it clear we parse it the right
/// way.
pub fn config(config: &Configuration, progress: &Progress) -> Result<(), std::io::Error> {
    progress.chatty("showing runtime configuration");

    let config = serde_json::to_vec_pretty(&config).unwrap();
    std::io::stdout().write_all(&config)?;
    Ok(())
}
