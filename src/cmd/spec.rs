//! The `spec` sub-command.

use std::io::Write;

use crate::progress::Progress;
use crate::spec::Specification;

/// The `spec` sub-command.
///
/// Write the actual VM specifications to stdout as JSON. We convert
/// the spec to JSON to make it more clear that we parse the config
/// and spec in the right way.
pub fn spec(specs: &[Specification], progress: &Progress) -> Result<(), std::io::Error> {
    progress.chatty("showing specification for virtual machines");

    let spec = serde_json::to_vec_pretty(specs).unwrap();
    std::io::stdout().write_all(&spec)?;
    std::io::stdout().write_all("\n".as_bytes())?;
    Ok(())
}
