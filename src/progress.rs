//! Show progress, or not.

use log::{debug, error, info};

#[derive(Debug, Clone, Copy)]
pub enum MessageKind {
    OnlyErrors,
    Steps,
    Everything,
}

pub struct Progress {
    level: MessageKind,
}

impl Progress {
    pub fn new(level: MessageKind) -> Self {
        Self { level }
    }

    fn message(&self, prefix: &str, msg: &str) {
        eprintln!("{}: {}", prefix, msg);
    }

    pub fn chatty(&self, msg: &str) {
        debug!("{}", msg);
        if let MessageKind::Everything = self.level {
            self.message("DEBUG", msg);
        }
    }

    pub fn step(&self, msg: &str) {
        match self.level {
            MessageKind::Everything | MessageKind::Steps => {
                info!("{}", msg);
                self.message("INFO", msg)
            }
            _ => (),
        }
    }

    pub fn error(&self, msg: &str) {
        // Errors are always written out.
        error!("{}", msg);
        self.message("ERROR", msg);
    }
}
