---
title: Release process for vmadm
...

# Release process for vmadm

* Land any changes that are in progress.
* Make sure `./check` runs successfully.
* Create a branch for making changes during release.
* Update `NEWS` to cover all user-visible changes.
* Choose new version number and update it in the relevant places.
  - `NEWS`
  - `Cargo.toml`
  - `debian/changelog`
* Commit version number changes.
* Create a release tag: `git tag -sam "Release X.Y.Z of vmadm" vX.Y.Z`
* Push release branch to `gitlab.com`, create a merge request, merge it.
* Change to the `main` branch locally, pull from `gitlab.com`.
* Push `main` branch to `git.liw.fi` so that CI builds things.
  - wait for that to happen
* Push tags to `git.liw.fi` so that CI builds release.
  - verify that it does
* Push tags to `gitlab.com` so they're there.
* Publish to `crates.io` with `cargo publish` after CI has built release.

If there's a problem after the push of the release branch to
`gitlab.com`, fix it, and then re-start release process from the
beginning.
