#!/bin/sh
#
# Run the automated tests for the project.

set -eu

hideok=chronic
if [ "$#" -gt 0 ]; then
	case "$1" in
	verbose | -v | --verbose)
		hideok=
		shift
		;;
	esac
fi

if [ "$#" = 0 ]; then
	echo "MUST give path to base image for testing" 1>&2
	exit 1
fi
base_image="$1"
shift

got_cargo_cmd() {
	cargo --list | grep " $1 " >/dev/null
}

$hideok cargo build --all-targets
got_cargo_cmd clippy && cargo clippy -q --all-targets -- -Dwarnings
got_cargo_cmd fmt && $hideok cargo fmt -- --check
$hideok cargo test

subplot docgen vmadm.subplot -o vmadm.html

subplot codegen vmadm.subplot -o test.py
rm -f test.log
$hideok python3 test.py --log test.log --env="BASE_IMAGE=$base_image" "$@"

echo "Everything seems to be in order."
